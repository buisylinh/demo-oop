Checklist code
Chức năng:
1: Nhập / xuất thông tin máy đơn  : done
2: Nhập / xuất thông tin máy phức : done
3: Nhập / xuất thông tin quản lí máy: done
4: Nhập / xuất thông tin quản lí kho : done

+ File để chạy code chạy lệnh "php demo.php"

+ Nhập thông tin của kho
		- Nhập danh sách máy có trong kho
		- Nhập danh sách chi tiết có trong máy
		- Nhập danh sách chi tiết có trong 1 chi tiết phức
+ Quản lý kho
		- Xuất danh sách máy có trong kho
		- Tính tổng khối lượng các máy có trong kho
		- Tính tổng tiền các máy có trong kho
		- Tìm máy theo mã nhập vào


Kiểm thử:
https://docs.google.com/spreadsheets/d/1ZDAhavhnNIcKdkxFDMsUoy8LBh7vkdOz2kQOfdn63d4/edit?usp=sharing

