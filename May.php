<?php

class May extends ChiTietMay {
    private $MaMay;
    private $TenMay;
    private $danhSachChiTiet = array();

    public function getMaSoMay() {
        return $this->MaMay;
    }

    public function Input() {
        echo "*--";
        $this->MaMay = readline("Nhập mã số máy: ");
        echo "*--";
        $this->TenMay = readline("Nhập tên máy: ");
        echo "*--";
        do {
            $danhSachChiTiet = readline("Nhập số lượng chi tiết: ");
            if (!is_numeric($danhSachChiTiet)) {
                echo "@@ Sai. Nhập lại đi anh (bằng số) \n";
            }
        } while (!is_numeric($danhSachChiTiet));

        for ($i = 0; $i < $danhSachChiTiet; $i++) {
            $loai = null;
            echo "*-- ";
            do {
                $loai = readline("Nhập loại chi tiết (1: Chi tiết đơn | 2: Chi tiết phức): ");
                if ($loai != 1 && $loai != 2) {
                    echo " @@ Sai. Nhập lại đi anh (bằng số) \n";
                }
            } while ($loai != 1 && $loai != 2);

            $chitiet = null;
            if ($loai == 1) {
                $chitiet = new ChiTietDon();
            } else {
                $chitiet = new ChiTietPhuc();
            }
            $chitiet->nhap();
            $this->danhSachChiTiet[] = $chitiet;
        }
    }

    public function Ouput() {
        echo "*--\n*-------Thông tin máy --------------*\n";
        echo "*--    Mã số máy : ".$this->MaMay."\n";
        echo "*--    Tên máy: ".$this->TenMay."\n";
        echo "*--    Danh sách chi tiết:    ";

        for ($i = 0; $i < count($this->danhSachChiTiet); $i++) {
            echo $this->danhSachChiTiet[$i]->getMaSo() . "      ";
        }
        echo "\n";
    }

    public function tinhTien() {
        $tongTien = 0;
        for ($i = 0; $i < count($this->danhSachChiTiet); $i++) {
            $tongTien += $this->danhSachChiTiet[$i]->tinhTien();
        }
        return $tongTien;
    }

    public function tinhKhoiLuong() {
        $tongKhoiLuong = 0;
        for ($i = 0; $i < count($this->danhSachChiTiet); $i++) {
            $tongKhoiLuong += $this->danhSachChiTiet[$i]->tinhKhoiLuong();
//            print_r($tongKhoiLuong);die;
        }
        return $tongKhoiLuong;
    }
    public function typeCheck($value, $type)
    {
        $value = trim($value, '    ');
        if ($type == 'int') {
            while ($value <= 0 || !ctype_digit($value)) {
                echo "*-----------------------------------------------------* \n";
                echo "*--Mời nhập lại. Phải là kiểu số dương nguyên (int) -_- \n";
                $value = readline("*--Lựa chọn của anh là (1 -> 5):  \n");

            }
        } elseif ($type == 'float') {
            while (!is_string($value) || !is_numeric($value) || $value < 0) {
                $value = readline('*--Mời nhập lại. Phải là int dương -_-');
            }
        } elseif ($type == 'string') {
            while (!is_string($value) || is_numeric($value)) {
                $value = readline('Mời nhập lại. Phải là kiểu string -_-');
            }
        } elseif ($type == 'yesNo') {
            while ((!is_numeric($value)) || ($value != 0 && $value != 1)) {
                $value = readline("Mời nhập lại. Phải là  1 hoặc 0 mới  được-_-");
            }
        } else {
            echo 'check type failed '."\n";
        }

        return $value;
    }
}

?>