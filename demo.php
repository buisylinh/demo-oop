<?php

include_once "ChiTietMay.php";
include_once "ChiTietDon.php";
include_once "ChiTietPhuc.php";
include_once "May.php";
include_once "Kho.php";
include_once "Typecheck.php";

echo "*---- QUẢN LÍ CHI TIẾT MÁY ---* \n";
echo "*                             * \n";
echo "*      1. Chi tiết Đơn        * \n";
echo "*      2. Chi tiết Phức       * \n";
echo "*      3. Quản lý Máy         * \n";
echo "*      4. Quản lý Kho         * \n";
echo "*      5. Thoát               * \n";
echo "*-----------------------------* \n";
$Check = new May();
$Chon = "";
echo "*-- ";
do {

    $Chon = readline("Lựa chọn của anh là (1 -> 5): ");
    $Chon = $Check->typeCheck($Chon,'int','float');
    echo "*------------------------------*\n";

    if (strval($Chon < 1 || $Chon > 5) ) {

        echo "Sai rồi anh, chọn từ số 1 tới 5 thôi -_- \n";
    }
} while ($Chon < 1 || $Chon > 5);
switch ($Chon) {
    case 1: {
        echo "*--  CHI TIẾT ĐƠN ---*\n";
        echo "*--                --* \n";

        $ChiTietDon = new ChiTietDon();
        $ChiTietDon->nhap();
        $ChiTietDon->xuat();
        break;
    }
    case 2: {
        echo "*--  CHI TIẾT PHỨC  --*\n";
        echo "*--\n";
        $ChiTietPhuc = new ChiTietPhuc();
        $ChiTietPhuc->nhap();
        $ChiTietPhuc->xuat();
        echo "*--------Tổng tiền và khối lượng-----------*\n";
        echo "*-- Tổng tiền: " . $ChiTietPhuc->tinhTien() . "\n";
        echo "*-- Tổng khối lượng: " . $ChiTietPhuc->tinhKhoiLuong() . "\n";
        echo "*--\n\n";
        break;
    }
    case 3: {
        echo "\n\n---\n";
        echo "*----- QUẢN LÝ MÁY ----*\n";
        echo "*---\n";
        $may = new May();
        $may->Input();
        $may->Ouput();
        echo "*--Tổng tiền và khối lượng máy ---*\n";
        echo "*--   Tổng tiền: " . $may->tinhTien() . "\n";
        echo "*--   Tổng khối lượng: " . $may->tinhKhoiLuong() . "\n";
        echo "*--\n\n";
        break;
    }
    case 4: {
        echo "\n\n---\n";
        echo "*---  QUẢN LÝ KHO  ---\n";
        echo "*---\n";
        $kho = new Kho();
        $kho->Input();
        echo "*-------------------------------*\n";
        $kho->thongKeDanhSachMay();
        echo "*-------------------------------*\n";
        $kho->tongGiaMay();
        $kho->tongKhoiLuongMay();
        $kho->timMay();
        echo "*---\n\n";
        break;
    }
    case 5:{
        echo " Hẹn gặp lại anh ^^ \n";
    }
}

?>