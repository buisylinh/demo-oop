<?php
class typeCheck{
    public function typeCheck($value, $type)
    {
        $value = trim($value, '    ');
        if ($type == 'int') {
            while ($value <= 0 || !ctype_digit($value)) {
                echo "*-----------------------------------------------------* \n";
                echo "*--Mời nhập lại. Phải là kiểu số dương nguyên (int) -_- \n";
                $value = readline("*--Lựa chọn của anh là (1 -> 5):  \n");

            }
        } elseif ($type == 'float') {
            while (!is_string($value) || !is_numeric($value) || $value < 0) {
                $value = readline('*--Mời nhập lại. Phải là int dương -_-');
            }
        } elseif ($type == 'string') {
            while (!is_string($value) || is_numeric($value)) {
                $value = readline('Mời nhập lại. Phải là kiểu string -_-');
            }
        } elseif ($type == 'yesNo') {
            while ((!is_numeric($value)) || ($value != 0 && $value != 1)) {
                $value = readline("Mời nhập lại. Phải là  1 hoặc 0 mới  được-_-");
            }
        } else {
            echo 'check type failed '."\n";
        }

        return $value;
    }
}