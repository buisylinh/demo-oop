<?php

class ChiTietPhuc extends ChiTietMay {
    private $dsChiTietMay = array();
    private $slChiTiet;

    public function nhap() {
        parent::nhap();
        echo "*-- ";
        do {
            $this->slChiTiet = readline("Nhập số lượng chi tiết: ");
            if (!is_numeric($this->slChiTiet)) {
                echo " @@ Sai. Nhập lại đi anh (bằng số) \n";
            }
        }
        while (!is_numeric($this->slChiTiet));
        
        for ($i = 0; $i < $this->slChiTiet; $i++) {
            $loaict = null;
            echo "*-- ";
            do {
                $loai = readline("Nhập loại chi tiết(1: Chi tiết đơn | 2: Chi tiết phức): ");
                if ($loaict != 1 && $loaict != 2) {
                    echo " @@ Sai. Nhập lại đi anh (bằng số) \n ";
                }
            }
            while ($loaict != 1 && $loaict != 2);

            $chitiet = null;
            if ($loaict == 2) {
                $chitiet = new ChiTietPhuc();
            } else {
                $chitiet = new ChiTietDon();
            }
            $chitiet->nhap();
            $this->dsChiTietMay[] = $chitiet;
        }
    }

    public function xuat() {
        echo"*-------------------------------------*\n";
        echo"*---- Thông tin chi tiết phức    --*";
        parent::xuat();
        for ($i = 0; $i < $this->slChiTiet; $i++) {
            $this->dsChiTietMay[$i]->xuat();
        }
    }


    function tinhTien() {
        $tongTien = 0;
        for ($i = 0; $i < $this->slChiTiet; $i++) {
            $tongTien += $this->dsChiTietMay[$i]->tinhTien();
        }
        return $tongTien;
    }

    function tinhKhoiLuong() {
        $tongKhoiLuong = 0;
        for ($i = 0; $i < $this->slChiTiet; $i++) {
            $tongKhoiLuong += $this->dsChiTietMay[$i]->tinhKhoiLuong();
        }
        return $tongKhoiLuong;
    }
}

?>